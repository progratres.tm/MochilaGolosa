package interfaz;
import java.util.Random;

import mochila.GeneradorRandom;
import mochila.Individuo;
import mochila.Instancia;
import mochila.Objeto;
import mochila.Poblacion;

public class Main
{
	public static void main(String[] args)
	{
		Instancia instancia = aleatoria(100);
		Individuo.setGenerador(new GeneradorRandom());
		Poblacion.setGenerador(new GeneradorRandom());
		
		Poblacion poblacion = new Poblacion(instancia);
		
		poblacion.registrarObservador(new ObservadorPorConsola(poblacion));
		poblacion.registrarObservador(new ObservadorVisual(poblacion));
		poblacion.simular();
	}

	private static Instancia aleatoria(int n)
	{
		Random random = new Random(2);
		Instancia instancia = new Instancia(1.5*n);
		
		for(int i=0; i<n; ++i)
		{
			int peso = random.nextInt(20);
			int beneficio = random.nextInt(10);

			instancia.agregarObjeto(new Objeto("x", peso, beneficio));
		}

		return instancia;
	}
}
