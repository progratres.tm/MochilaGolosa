package mochila;

public class TestClass
{
	public static void main(String[] args)
	{
		Instancia instancia = new Instancia(50);
		instancia.agregarObjeto(new Objeto("A", 10, 100));
		instancia.agregarObjeto(new Objeto("B", 20, 80));
		instancia.agregarObjeto(new Objeto("C", 15, 45));
		instancia.agregarObjeto(new Objeto("D", 10, 50));
		
		SolverGoloso solver = new SolverGoloso(instancia);
		
		System.out.println(solver.resolver(FactoryComparator.porBeneficio()));
		System.out.println(solver.resolver(FactoryComparator.porPeso()));		
		System.out.println(solver.resolver(FactoryComparator.porCociente()));		
	}
}
