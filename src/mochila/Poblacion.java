package mochila;

import java.util.ArrayList;
import java.util.Collections;

public class Poblacion
{
	// Variables de instancia
	private Instancia _instancia;
	private ArrayList<Individuo> _individuos;
	
	// Par�metros de la poblaci�n
	private int _tama�o = 100;
	private int _mutadosPorIteracion = 10;
	private int _recombinacionesPorIteracion = 20;
	private int _eliminadosPorIteracion = 50;
	
	// La poblaci�n es subject de observadores
	private ArrayList<Observador> _observadores;
	
	// El generador de n�meros aleatorios es static para usar un �nico generador
	private static Generador _random = null;
	
	// Setter para el generador aleatorio
	public static void setGenerador(Generador generador)
	{
		_random = generador;
	}
	
	public Poblacion(Instancia instancia)
	{
		_instancia = instancia;
		_observadores = new ArrayList<Observador>();
	}
	
	public void registrarObservador(Observador observador)
	{
		_observadores.add(observador);
	}

	public Individuo simular()
	{
		inicializarAleatoriamente();
		notificarObservadores();

		for (int i=0; i<1000;i++)
		{
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			completarConAleatorios();
			notificarObservadores();
		}
		
		return mejorIndividuo();
	}

	private void inicializarAleatoriamente()
	{
		_individuos = new ArrayList<Individuo>();
		for(int i=0; i<_tama�o; ++i)
			_individuos.add( Individuo.aleatorio(_instancia) );
	}

	private boolean satisfactoria()
	{
		// TODO: Implementar!
		return false;
	}

	private void mutarAlgunos()
	{
		for(int i=0; i<_mutadosPorIteracion; ++i)
		{
			Individuo seleccionado = aleatorio();
			seleccionado.mutar();
		}		
	}

	private void recombinarAlgunos()
	{
		for(int i=0; i<_recombinacionesPorIteracion; ++i)
		{
			Individuo padre1 = aleatorio();
			Individuo padre2 = aleatorio();
			
			for(Individuo hijo: padre1.recombinar(padre2))
				_individuos.add(hijo);
		}
	}

	private Individuo aleatorio()
	{
		int j = _random.nextInt(_tama�o);
		return _individuos.get(j);
	}

	private void eliminarPeores()
	{
		ordenarPorFitness();
		for(int i=0; i<_eliminadosPorIteracion; ++i)
			_individuos.remove( _individuos.size()-1 );
	}

	private void completarConAleatorios()
	{
		while( _individuos.size() < _tama�o )
			_individuos.add( Individuo.aleatorio(_instancia) );
	}
	
	private void notificarObservadores()
	{
		for(Observador observador: _observadores)
			observador.notificar();
	}

	public Individuo mejorIndividuo()
	{
		ordenarPorFitness();
		return _individuos.get(0);
	}

	public Individuo peorIndividuo()
	{
		ordenarPorFitness();
		return _individuos.get( _individuos.size()-1 );
	}

	// Ordenamos los individuos de mayor a menor fitness
	private void ordenarPorFitness()
	{
		Collections.sort(_individuos, (i1,i2) -> (int)(i2.fitness() - i1.fitness()));
	}

	// Fitness promedio de la poblaci�n
	public double fitnessPromedio()
	{
		double suma = 0;
		for(Individuo individuo: _individuos)
			suma += individuo.fitness();
		
		return suma / _individuos.size();
	}
}
