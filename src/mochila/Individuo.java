package mochila;

//Representa un individuo en la poblaci�n del algoritmo gen�tico
public class Individuo
{
	// Un individuo se representa por una secuencia de bits, uno por cada objeto
	private boolean[] _bits;
	
	// Instancia
	private Instancia _instancia;
	
	// El generador de n�meros aleatorios es static para usar un �nico generador
	private static Generador _random = null;
	
	// Setter para el generador aleatorio
	public static void setGenerador(Generador generador)
	{
		_random = generador;
	}

	// Factory para construir individuos aleatorios
	public static Individuo aleatorio(Instancia instancia)
	{
		Individuo ret = new Individuo(instancia);
			
		for(int i=0; i<instancia.cantidadDeObjeto(); ++i)
			ret.set(i, _random.nextBoolean());
		
		return ret;
	}
	
	// Factory para construir individuos con todos los bits en false
	public static Individuo vacio(Instancia instancia)
	{
		return new Individuo(instancia);
	}

	// El constructor es privado
	private Individuo(Instancia instancia)
	{
		_bits = new boolean[instancia.cantidadDeObjeto()];
		_instancia = instancia;
	}

	// Mutaci�n del individuo: Seleccionamos un bit aleatoriamente y lo cambiamos
	public void mutar()
	{
		int i = _random.nextInt(_bits.length);
		_bits[i] = !_bits[i];
	}
	
	// Recombinaci�n de dos individuos
	public Individuo[] recombinar(Individuo otro)
	{
		Individuo hijo1 = Individuo.vacio(_instancia);
		Individuo hijo2 = Individuo.vacio(_instancia);
		
		int corte = _random.nextInt(_bits.length);
		for(int i=0; i<corte; ++i)
		{
			hijo1.set(i, this.get(i));
			hijo2.set(i, otro.get(i));
		}
		
		for(int i=corte; i<_bits.length; ++i)
		{
			hijo1.set(i, otro.get(i));
			hijo2.set(i, this.get(i));
		}
		
		return new Individuo[] { hijo1, hijo2 };
	}
	
	// El fitness es negativo si no cumple la capacidad de la mochila
	public double fitness()
	{
		if( this.peso() <= _instancia.getPesoMaximo() )
			return this.beneficio();
		else
			return - this.peso() + _instancia.getPesoMaximo();		
	}
	
	// Peso y beneficio de un individuo
	public double peso()
	{
		// TODO: Testear!
		double ret = 0;
		for(int i=0; i<_instancia.cantidadDeObjeto(); ++i) if( get(i) == true )
			ret += _instancia.objeto(i).peso();
		
		return ret;
	}
	public double beneficio()
	{
		// TODO: Testear!
		double ret = 0;
		for(int i=0; i<_instancia.cantidadDeObjeto(); ++i) if( get(i) == true )
			ret += _instancia.objeto(i).beneficio();
		
		return ret;
	}
	
	// Getter y setter de los bits
	boolean get(int i)
	{
		return _bits[i];
	}
	void set(int i, boolean bit)
	{
		_bits[i] = bit;
	}
}
