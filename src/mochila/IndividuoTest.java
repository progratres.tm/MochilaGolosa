package mochila;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IndividuoTest 
{
	private Instancia _instancia;
	
	@Before	
	public void inicializar()
	{
		_instancia = new Instancia(10);
		_instancia.agregarObjeto(new Objeto("obj1", 4, 2));
		_instancia.agregarObjeto(new Objeto("obj2", 7, 4));
		_instancia.agregarObjeto(new Objeto("obj3", 9, 6));
		_instancia.agregarObjeto(new Objeto("obj4", 2, 4));
		_instancia.agregarObjeto(new Objeto("obj5", 1, 1));	
	}
	
	@Test
	public void mutarTest()
	{
		testearMutacion("10110", 2, "10010");
	}
	
	
	@Test
	public void mutarBorde0Test()
	{
		testearMutacion("10110", 0, "00110");
	}
 
	@Test
	public void mutarBordenTest()
	{
		testearMutacion("10110", 4, "10111");
	}
 
	@Test
	public void recombinarTest()
	{
		testearRecombinacion("10110", "01001", 2, "10001", "01110");
	}
 
	private void testearMutacion(String original, int bit, String resultado)
	{
		GeneradorPrefijado generador = new GeneradorPrefijado(convertir(original), bit);
 
		Individuo.setGenerador(generador);
		Individuo individuo = Individuo.aleatorio(_instancia);
		individuo.mutar();
		
		assertIguales(resultado, individuo);
	}
	
	private void testearRecombinacion(String padre1, String padre2, int punto, String hijo1, String hijo2)
	{
		GeneradorPrefijado generador = new GeneradorPrefijado(convertir(padre1+padre2+"0000000000"), punto);
		Individuo.setGenerador(generador);
		
		Individuo padre = Individuo.aleatorio(_instancia);
		Individuo madre = Individuo.aleatorio(_instancia);
		Individuo[] hijos = padre.recombinar(madre);
		
		assertIguales(hijo1, hijos[0]);
		assertIguales(hijo2, hijos[1]);
	}
		
	private boolean[] convertir(String bits)
	{
		boolean[] ret = new boolean[bits.length()];
		for(int i=0; i<bits.length(); ++i)
			ret[i] = bits.charAt(i) == '1';
		
		return ret;
	}
 
	private void assertIguales(String bitsEnString, Individuo individuo)
	{
		boolean[] bitsResultado = convertir(bitsEnString);
		for(int i=0; i<bitsEnString.length(); ++i)
			assertEquals(bitsResultado[i], individuo.get(i));
	}

}
