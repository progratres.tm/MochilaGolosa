package mochila;
import java.util.ArrayList;

public class Instancia
{
	private double _pesoMaximo;
	private ArrayList<Objeto> _objetos;
	
	public Instancia(double peso)
	{
		_pesoMaximo = peso;
		_objetos = new ArrayList<Objeto>();
	}
	
	public void agregarObjeto(Objeto obj)
	{
		_objetos.add(obj);
	}
	
	public double getPesoMaximo()
	{
		return _pesoMaximo;
	}
	
	public int cantidadDeObjeto()
	{
		return _objetos.size();
	}
	
	public Objeto objeto(int i)
	{
		return _objetos.get(i);
	}

	public ArrayList<Objeto> objetos() 
	{
		ArrayList<Objeto> ret = new ArrayList<Objeto>();
		
		for(int i=0; i<cantidadDeObjeto(); ++i)
			ret.add( objeto(i) );
		
		return ret;
	}
	
	public double peso(int i)
	{
		return objeto(i).peso();
	}
	
	public double beneficio(int i)
	{
		return objeto(i).beneficio();
	}
	
}
