package mochila;

import java.util.Random;

public class GeneradorRandom implements Generador
{
	private Random _random;
	
	public GeneradorRandom()
	{
		_random = new Random();
	}
	
	public boolean nextBoolean() 
	{
		return _random.nextBoolean();
	}

	public int nextInt(int valor) 
	{
		return _random.nextInt(valor);
	}
}
