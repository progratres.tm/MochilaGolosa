package mochila;

public class GeneradorPrefijado implements Generador
{
	private boolean[] _booleanos;
	private int _entero;
	private int _indice; // por qu� booleano vamos
	
	public GeneradorPrefijado(boolean[] booleanosPrefijados, int enteroPrefijado)
	{
		_booleanos = booleanosPrefijados;
		_entero = enteroPrefijado;
		_indice = 0;
		
	}
	
	public boolean nextBoolean() 
	{
		return _booleanos[_indice++];
	}

	public int nextInt(int limite) 
	{
		return _entero;
	}

}
