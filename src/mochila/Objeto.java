package mochila;

public class Objeto
{
	private String _nombre;
	private double _peso;
	private double _beneficio;
	
	public Objeto(String n, double p, double b)
	{
		_nombre = n;
		_peso = p;
		_beneficio = b;
	}
	
	public String nombre()
	{
		return _nombre;
	}
	
	public double peso()
	{
		return _peso;
	}
	
	public double beneficio()
	{
		return _beneficio;
	}
	
	public double cociente()
	{
		return _beneficio / _peso;
	}
}
