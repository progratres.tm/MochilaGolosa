package mochila;
import java.util.HashSet;
import java.util.Set;

public class Subconjunto
{
	private Instancia _instancia;
	private Set<Objeto> _objetos;
	
	public Subconjunto(Instancia instancia)
	{
		_instancia = instancia;
		_objetos = new HashSet<Objeto>();
	}
	
	public void addObjeto(Objeto obj)
	{
		_objetos.add(obj);
	}
	
	public boolean entra(Objeto obj)
	{
		return this.getPeso() + obj.peso()
		   <= _instancia.getPesoMaximo();
	}
	
	public double getPeso()
	{
		double ret = 0;
		for(Objeto obj: _objetos)
			ret += obj.peso();
		
		return ret;
	}
	
	@Override public String toString()
	{
		String ret = "";
		for(Objeto obj: _objetos)
			ret += obj.nombre() + " ";
		
		return ret;
	}
}
