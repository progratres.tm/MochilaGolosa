package mochila;

import java.util.Comparator;

public class FactoryComparator 
{
	public static Comparator<Objeto> porPeso()
	{
		return (uno, otro) -> (int) (uno.peso() - otro.peso());
	}

	public static Comparator<Objeto> porBeneficio()
	{
		return (uno, otro) -> (int) (otro.beneficio() - uno.beneficio());
	}
	
	public static Comparator<Objeto> porCociente()
	{
		return (uno, otro) -> (int) (uno.cociente() - otro.cociente());
	}
}
